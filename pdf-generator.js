const fs = require("fs");
const path = require("path");
const puppeteer = require('puppeteer');
const handlebars = require("handlebars");

const toTreat = 10;

let globalBrowser;


const minimal_args = [
	'--autoplay-policy=user-gesture-required',
	'--disable-background-networking',
	'--disable-background-timer-throttling',
	'--disable-backgrounding-occluded-windows',
	'--disable-breakpad',
	'--disable-client-side-phishing-detection',
	'--disable-component-update',
	'--disable-default-apps',
	'--disable-dev-shm-usage',
	'--disable-domain-reliability',
	'--disable-extensions',
	'--disable-features=AudioServiceOutOfProcess',
	'--disable-hang-monitor',
	'--disable-ipc-flooding-protection',
	'--disable-notifications',
	'--disable-offer-store-unmasked-wallet-cards',
	'--disable-popup-blocking',
	'--disable-print-preview',
	'--disable-prompt-on-repost',
	'--disable-renderer-backgrounding',
	'--disable-setuid-sandbox',
	'--disable-speech-api',
	'--disable-sync',
	'--hide-scrollbars',
	'--ignore-gpu-blacklist',
	'--metrics-recording-only',
	'--mute-audio',
	'--no-default-browser-check',
	'--no-first-run',
	'--no-pings',
	'--no-sandbox',
	'--no-zygote',
	'--password-store=basic',
	'--use-gl=swiftshader',
	'--use-mock-keychain',
  ];

async function createPDF(data, callback){
	var templateHtml = fs.readFileSync(path.join(process.cwd(), 'template.html'), 'utf8');

	var template = handlebars.compile(templateHtml);
	var html = template(data);

	var milis = new Date();
	milis = milis.getTime();

	var pdfPath = path.join('pdf', `${data.name}-${milis}.pdf`);

	var options = {
		displayHeaderFooter: false,
		format: 'A4',
		printBackground: true,
		path: pdfPath
	}

	// let browser;
	// if (globalBrowser == null) {
	// 	browser = await puppeteer.launch({
	// 		args: ['--no-sandbox'],
	// 		headless: true
	// 	});
	// } else {
	
	// }
	browser = globalBrowser;
	// const browser = await puppeteer.connect({
	// 	browserWSEndpoint: `ws://localhost:3000`,
	// });

	var page = await browser.newPage();

	await page.setContent(html, {
		waitUntil: ["networkidle0"]
	});
	

	await page.pdf(options);
	await page.close();
	// await browser.close();
	callback(null, 'DONE');

}

const data = {
	title: "A new Brazilian School",
	date: "05/12/2018",
	name: "Rodolfo Luis Marcos",
	age: 28,
	birthdate: "12/07/1990",
	course: "Computer Science",
	obs: "Graduated in 2014 by Federal University of Lavras, work with Full-Stack development and E-commerce."
}


async function bench() {
	console.log(`Let's go for ${toTreat} pdf`);
	console.time("bench");
	globalBrowser = await puppeteer.launch({
		args: minimal_args,
		userDataDir: './cache/pupetter',
		headless: true
	});

	var parallelLimit = require('run-parallel-limit')

	var tasks = [];

	for (let index = 0; index < toTreat; index++) {
		tasks.push((callback) => createPDF(data, callback));
	}
	
	parallelLimit(tasks, 8, function (err, results) {
		console.timeEnd("bench");
		globalBrowser.close();
	})

}	

bench();	
